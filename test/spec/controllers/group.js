'use strict';

describe('Controller: AccountCtrl', function() {

	// load the controller's module
	beforeEach(module('ngMaterialDashboardUser'));

	var AmdGroupCtrl;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$usr_) {
		scope = $rootScope.$new();
		AmdGroupCtrl = $controller('AmdGroupCtrl', {
			$scope : scope,
			$usr : _$usr_
			// place here mocked dependencies
		});
	}));

	it('should be defined', function() {
		expect(angular.isDefined(AmdGroupCtrl)).toBe(true);
	});
});
